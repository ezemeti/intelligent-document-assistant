import requests
import json

post_url = "https://cn.bing.com/ttranslatev3?isVertical=1&&IG=3B6B683AF4924F13AB99DF4F446D175D&IID=translator.5026"
headers = {
    "User-Agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) "
                  "Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/118.0.0.0"
}
with open("Tran_data.json","r",encoding="utf-8") as f:
    Data = json.load(f)


def translate(text):
    data = {
        "fromLang": Data["fromLang"],
        "text": text,
        "to": Data["to"],
        "token": Data["token"],
        "key": Data["key"],
        "tryFetchingGenderDebiasedTranslations": "true"
    }
    resp = requests.post(url=post_url, headers=headers, data=data)
    result = resp.json()
    # 查看result值
    # print(result)

    # print(result[0]['translations'][0]['text'])
    try:
        return result[0]['translations'][0]['text']
    except:
        return "请修改key和token值!"



