# 智能文档助手

#### 介绍
使用python接入讯飞火星和微软，制作的智能文档助手

#### 软件架构
![输入图片说明](img640.png)


#### 安装教程

```python
# 使用清华镜像
pip install -i https://pypi.tuna.tsinghua.edu.cn/simple -r re.txt
```


#### 使用说明
讯飞火星的API接口配置到Chat_data.json文件中。

```JavaScript
{
  "appid" : "appid值",
"api_secret" : "api_secret值",
"api_key" : "api_key值"
}
```
微软翻译的token和key配置到Tran_data.json文件中。

```JavaScript
{
  "fromLang": "源文件语言",
  "to": "目标语言",
  "token": "token",
  "key": "key",
  "tryFetchingGenderDebiasedTranslations": "true"
}
```
微软翻译的token和key获取在浏览器开发工具中，   
![输入图片说明](.idea/640%20(1).png)


#### 其他教程在公众号中
公众号名 “ 索隆程序员 ”   
![输入图片说明](.idea/qrcode_for_gh_f8040bc6db2e_344.jpg)



