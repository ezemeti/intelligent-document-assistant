import argparse

import Read
import Read_Translate
import test
import Document_AI
import Tran


def main():
    parser = argparse.ArgumentParser(description="翻译文档、AI聊天、AI分析文档的软件")

    # 参数1：文档路径
    parser.add_argument('--document', '-d', action='store_true', help='要翻译文档')

    # 参数2：翻译语言
    parser.add_argument('--translate', '-t', action='store_true', help='要翻译的目标语言')

    # 参数3：AI聊天模式
    parser.add_argument('--chat', '-c', action='store_true', help='进入AI聊天模式')

    # 参数4：文档分析模式
    parser.add_argument('--analyze', '-a', action='store_true', help='进入文档分析模式')

    args = parser.parse_args()

    if args.document:
        Read_Translate.Tren_main()
    elif args.translate:
        Tran.main()
    elif args.chat:
        test.main()
    elif args.analyze:
        Read.main()


if __name__ == '__main__':
    main()
